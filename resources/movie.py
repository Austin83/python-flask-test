from flask import Response, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from database.models import Movie, User
from flask_restful import Resource

from mongoengine.errors import FieldDoesNotExist, NotUniqueError, DoesNotExist, ValidationError, InvalidQueryError
from resources.errors import SchemaValidationError, MovieAlreadyExistsError, InternalServerError, UpdatingMovieError, DeletingMovieError, MovieNotExistsError

class MoviesApi(Resource):
    def get(self):
        movies = Movie.objects().to_json()
        return Response(movies, mimetype="application/json", status=200)

    @jwt_required
    def post(self):
        try:
            user = User.objects.get(id=get_jwt_identity())
            movie = Movie(**request.get_json(), added_by=user)
            movie.save()
            user.update(push__movies=movie)
            user.save()
            return Response(str(movie.id), mimetype='application/json', status=200)
        except (FieldDoesNotExist, ValidationError):
            raise SchemaValidationError
        except NotUniqueError:
            raise MovieAlreadyExistsError
        except Exception as e:
            raise InternalServerError

class MovieApi(Resource):
    def get(self, id):
        try:
            movie = Movie.objects.get(id=id).to_json()
            return Response(movie, mimetype='application/json', status=200)
        except DoesNotExist:
            raise MovieNotExistsError
        except Exception:
            raise InternalServerError
    
    @jwt_required
    def put(self, id):
        try:
            Movie.objects.get(id=id, added_by=get_jwt_identity()).update(**request.get_json())
            return Response('', mimetype="application/json", status=200)
        except InvalidQueryError:
            raise SchemaValidationError
        except DoesNotExist:
            raise UpdatingMovieError
        except Exception:
            raise InternalServerError

    @jwt_required
    def delete(self, id):
        try:
            Movie.objects.get(id=id, added_by=get_jwt_identity()).delete()
            return Response('', mimetype="application/json", status=200)
        except DoesNotExist:
            raise DeletingMovieError
        except Exception:
            raise InternalServerError
            