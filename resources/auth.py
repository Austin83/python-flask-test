from flask import Response, request
from flask_jwt_extended import create_access_token
from database.models import User
from flask_restful import Resource
import datetime
from mongoengine.errors import FieldDoesNotExist, NotUniqueError, DoesNotExist
from resources.errors import SchemaValidationError, EmailAlreadyExistsError, UnauthorizedError, InternalServerError

class SignupApi(Resource):
    def post(self):
        try:
            user = User(**request.get_json())
            user.hash_password()
            user.save()
            return str(user.id), 200
        except FieldDoesNotExist:
            raise SchemaValidationError
        except NotUniqueError:
            raise EmailAlreadyExistsError
        except Exception as e:
            raise InternalServerError    

class LoginApi(Resource):
    def post(self):
        try:
            user = User.objects.get(email=request.get_json().get('email'))
            if not user.check_password(request.get_json().get('password')):
                return {'error':'Email or password invalid'}, 401
            expire=datetime.timedelta(minutes=30)
            access_token = create_access_token(identity=str(user.id), expires_delta=expire)    
            return {'token': access_token}, 200
        except (UnauthorizedError, DoesNotExist):
            raise UnauthorizedError
        except Exception as e:
            raise InternalServerError
            
