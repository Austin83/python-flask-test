from app import app
from flask_restful import Api
from resources.routes import initialize_routes
from resources.errors import errors


api = Api(app, errors=errors)
initialize_routes(api)
app.run()

