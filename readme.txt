This is a python+flask sample proyect that follows the following tutorial series
https://dev.to/paurakhsharma/flask-rest-api-part-0-setup-basic-crud-api-4650

It's basically a statement of progress.
After the tutorial is finished, more will be added to improve the API.

Dev's notes:

python3 -m pipenv --python 3.6	
python3 -m pipenv install whatever
python3 -m pipenv shell
pip install wheel (before flask_mongoengine)
o
python3 -m pipenv install wheel
add private key in a .env file at root of proyect JWT_SECRET_KEY = ''
export ENV_FILE_LOCATION=./.env
add configuration of mail server at in .env file

    MAIL_SERVER: "localhost"
    MAIL_PORT = "1025"
    MAIL_USERNAME = "support@movie-bag.com"
    MAIL_PASSWORD = ""

use the next command to test the SMTP (email) server
    python -m smtpd -n -c DebuggingServer localhost:1025

MONGODB_SETTINGS = {
    'db':'moviebag',
    'username':'',
    'password':'',
    'port':27017,
    'host':'localhost',
    'authentication_source':'admin'
}

