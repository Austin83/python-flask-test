from flask import Flask
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_mail import Mail

from database.db import initialize_db

app = Flask(__name__)
app.config.from_envvar('ENV_FILE_LOCATION')
bcrypt = Bcrypt(app)
jwt = JWTManager(app)
mail = Mail(app)

initialize_db(app)

